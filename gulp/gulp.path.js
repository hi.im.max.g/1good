import { themeName } from './gulp.config';

const assets = './themes/' + themeName + '/assets/';

export const path = {
    html: {
        src: [
            './themes/' + themeName + '/pages/*.htm',
            './themes/' + themeName + '/pages/**/*.htm',
            './themes/' + themeName + '/content/*.htm',
            './themes/' + themeName + '/content/**/*.htm',
            './themes/' + themeName + '/partials/*.htm',
            './themes/' + themeName + '/partials/**/*.htm',
            './themes/' + themeName + '/layouts/*.htm',
        ]
    },
    style: {
        src: {
            main: assets + 'scss/style.scss',
            vendor: assets + 'scss/vendor.scss',
        },
        dist: assets + 'css/',
        watch: [
            assets + 'scss/*.scss',
            assets + 'scss/sections/*.scss',
            assets + 'scss/components/*.scss',
        ]
    },
    script: {
        src: {
            main: assets + 'js/app.js',
            vendor: assets + 'js/vendor.js',
        },
        dist: assets + 'script/',
        watch: [
            assets + 'js/*.js'
        ]
    },
};