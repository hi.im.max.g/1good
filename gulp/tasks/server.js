import bs       from 'browser-sync';

export const Serve = () => {
    bs.init({
      proxy: "http://127.0.0.1:8000/",
      tunnel: "agro-group"
    })
}
