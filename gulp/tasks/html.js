import { path } from '../gulp.path';
import { src } from 'gulp';
import env              from 'gulp-environment';
import bs               from 'browser-sync';

export const Html = (done) => {
    src(path.html.src)
        .pipe(env.if.development(bs.stream()))
        done();
}