import { src, dest }    from 'gulp';
import { path }         from '../gulp.path';
import env              from 'gulp-environment';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';
import webpackStream    from 'webpack-stream';
import bs               from 'browser-sync';
import webpackConfig     from '../../webpack.config';

export const Script = (done) => {
    src(path.script.src.main)
        .pipe(plumber({errorHandler: notify.onError("JavaScript: <%= error.message %>")}))
        .pipe(webpackStream(webpackConfig))
        .pipe(dest(path.script.dist))
        .pipe(env.if.development(bs.reload({stream: true})))
        done();
}