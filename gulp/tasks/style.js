import { path } from '../gulp.path';
import { src, dest } from 'gulp';
import env              from 'gulp-environment';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';
import bs               from 'browser-sync';
import sourcemaps       from 'gulp-sourcemaps';
import postcss          from 'gulp-postcss';
import autoprefixer     from 'autoprefixer';
import cssnano          from 'cssnano';
import scss             from 'gulp-sass';

const plugins = [
    autoprefixer(),
    cssnano()
];

export const Style = (done) => {
    src(path.style.src.main)
        .pipe(plumber({errorHandler: notify.onError("Error pug: <%= error.message %>")}))
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(scss())
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('../maps'))
        .pipe(dest(path.style.dist))
        .pipe(env.if.development(bs.stream()))
        done();
}

export const StyleVendor = (done) => {
    src(path.style.src.vendor)
        .pipe(plumber({errorHandler: notify.onError("Error pug: <%= error.message %>")}))
        .pipe(scss())
        .pipe(postcss(plugins))
        .pipe(dest(path.style.dist))
        .pipe(env.if.development(bs.stream()))
        done();
}