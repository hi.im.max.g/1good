import { watch, parallel }      from 'gulp';
import { path }                 from '../gulp.path';
import { Style, StyleVendor }                from './style';
import { Script }               from './script';
import { Html } from './html';

export const WatchFiles = (done) => {
  watch(path.style.watch, parallel(Style, StyleVendor));
  watch(path.script.watch, parallel(Script));
  watch(path.html.src, parallel(Html));
  done();
}
