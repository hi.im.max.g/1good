import path                     from 'path';
import { CleanWebpackPlugin }   from 'clean-webpack-plugin';
import env                      from 'gulp-environment';
import { themeName }            from './gulp/gulp.config';

const production = env.production;
const webpackENV = {
    mode: production ? 'production' : 'development',
    devtools: production ? 'none' : 'source-map'
};

module.exports = {
    mode: webpackENV.mode,
    entry: {
        app: './themes/' + themeName + '/assets/js/app.js',
        vendor: './themes/' + themeName + '/assets/js/vendor.js',
    },
    output: {
        path: path.resolve(__dirname, './themes/' + themeName + '/assets/script/'),
        filename: '[name].bundle.js'
    },
    plugins: [
        new CleanWebpackPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
        ]
    },
    devtool: webpackENV.devtools
}