import { parallel, series }             from 'gulp';
import { Serve }                        from './gulp/tasks/server';
import { WatchFiles }                   from './gulp/tasks/watch';
import { Style, StyleVendor  }          from './gulp/tasks/style';
import { Script }                       from './gulp/tasks/script';
import { Html }                         from './gulp/tasks/html';

// Запуск сервера и watch
let RunServer = parallel(
    WatchFiles,
    Serve
)

// Development-сборка
let Build = series(
    Html,
    parallel(
        StyleVendor,
        Style
    ),
    Script,
    RunServer
);

// Production-сборка
let BuildProduction = series(
    Html,
    parallel(
        StyleVendor,
        Style
    ),
    Script
);

exports.default     = Build;
exports.production  = BuildProduction;